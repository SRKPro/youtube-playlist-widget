// OM GANESHAY NAMAH //

package com.om.youtubeplaylistwidget;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;

import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.om.youtubeplaylistwidget.Retrofit.IMyAPI;
import com.om.youtubeplaylistwidget.Retrofit.IMyChannelAPI;
import com.om.youtubeplaylistwidget.Retrofit.RetrofitClient;
import com.om.youtubeplaylistwidget.models.ModelPlaylist;
import com.om.youtubeplaylistwidget.models.PlaylistDetail;
import com.om.youtubeplaylistwidget.models.channel.ModelChannel;
import com.om.youtubeplaylistwidget.models.getid.NewModelid;
import com.om.youtubeplaylistwidget.models.video.ModelVideo;
import com.om.youtubeplaylistwidget.models.video.VideoDetail;
import com.om.youtubeplaylistwidget.network.YoutubeChannelAPI;
import com.om.youtubeplaylistwidget.network.YoutubePlaylistAPI;
import com.om.youtubeplaylistwidget.network.YoutubeVideoAPI;


import java.io.FileDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private BillingClient billingClient;

    private TextView textViewResult;
    EditText editText;

    String content = "";
    public static String link = "";
    public static String url = "";
    String imageUrl = "";
    ImageView imageView;
    Button buttonView;
    IMyAPI myAPI;
    IMyChannelAPI myChannelAPI;
    String[] taa1;
    String urlChannel = YoutubeChannelAPI.BASE_URL + YoutubeChannelAPI.type + YoutubeChannelAPI.part + YoutubeChannelAPI.channel + YoutubeChannelAPI.KEY;
    String channelName;
    private Boolean bought;

    CompositeDisposable compositeDisposable = new CompositeDisposable();



    @Override
    protected void onResume() {
        super.onResume();
//
//
//        editText = findViewById(R.id.edited);
//       // editText.getText().clear();
//        editText.setText("");
//
//
//        Context context = getApplicationContext();
//
//        ComponentName myProvider =
//                new ComponentName(context, Playlist.class);
//
//        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);
//
//        int widgetId;
//
//        if (ids.length > 1) {
//            widgetId = ids[ids.length - 1] + 1;
//            Log.d("id", "ids.length > 1" + widgetId);
//        } else if (ids.length == 1) {
//            widgetId = ids[0] + 1;
//            Log.d("id", "ids.length == 1 " + widgetId);
//        } else {
//            widgetId = 999;
//            Log.d("id", "else" + widgetId);
//        }
//
//        SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);
//        SharedPreferences.Editor prefEditor = setting.edit();
//
//        Bundle extras = getIntent().getExtras();
//
//
//        if (extras != null) {
//           // editText = findViewById(R.id.edited);
//            Toast.makeText(MainActivity.this, editText.getText() + "Hi", Toast.LENGTH_SHORT).show();
//           // EditText editText = findViewById(R.id.edited);
//            Log.d("The 2 :", "In Extra");
//            link = extras.getString(Intent.EXTRA_TEXT);
//
//            prefEditor.putString("playlistLink" + widgetId, link);
//            prefEditor.apply();
//            if (link != null) {
//
//                Log.d("The Link :", link);
//            }
//
//          //  editText.getText().clear();
//            editText.setText(link);
//           // getJson();
//        }


    }


    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }


    private Observable<NewModelid> modelidObservable() {

        return RetrofitClient.getMyAPI().getId(channelName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Function<NewModelid, ObservableSource<NewModelid>>() {
                    @Override
                    public ObservableSource<NewModelid> apply(NewModelid newModelid) throws Exception {
                        return Observable.just(newModelid)
                                .subscribeOn(Schedulers.io());
                    }
                });
    }

    private Observable<ModelChannel> getChannelObservable(final NewModelid newModelid) {

        return RetrofitClient.getMyAPI()
                .getYT(newModelid.items.get(0).snippet.channelId)
                .map((Function<ModelChannel, ModelChannel>) modelChannel -> modelChannel)
                .subscribeOn(Schedulers.io());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Activity activity = this;


        buttonView = findViewById(R.id.addWidget);
        buttonView.setOnClickListener(v -> {
            addWidget();
        });
        editText = findViewById(R.id.edited);
        textViewResult = findViewById(R.id.textView1);
        imageView = findViewById(R.id.testImage);
        buttonView.setEnabled(false);

        final Button loadButton = (Button) findViewById(R.id.button);
        loadButton.setOnClickListener(v -> {

            if (!Settings.System.canWrite(getApplicationContext()))
            {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getApplicationContext().getApplicationInfo().packageName));
                startActivity(intent);
                return;
            }

            try {
                getJson();
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(MainActivity.this);
                } else {
                    Log.d("Ad-Test", "The interstitial ad wasn't ready yet.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        /* init APIs */

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(@NonNull InitializationStatus initializationStatus)
            {

            }
        });

        loadVideoAd();

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

         loadInterstitialAd();

        //Initialize a BillingClient with PurchasesUpdatedListener onCreate method

//        billingClient = BillingClient.newBuilder(getApplicationContext())
//                .setListener(new PurchasesUpdatedListener() {
//                    @Override
//                    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
//                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
//                            for (Purchase purchase : list) {
//                                verifyPayment(purchase);
//                            }
//                        }
//
//                    }
//                })
//                .enablePendingPurchases()
//                .build();

        Context context = getApplicationContext();

        ComponentName myProvider =
                new ComponentName(context, Playlist.class);


        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);

        int widgetId;

        if (ids.length > 1) {
            widgetId = ids[ids.length - 1] + 1;
            Log.d("idonCreate", "ids.length > 1" + widgetId);
        } else if (ids.length == 1) {
            widgetId = ids[0] + 1;
            Log.d("id", "ids.length == 1 " + widgetId);
        } else {
            widgetId = 999;
            Log.d("id", "else" + widgetId);
        }

        SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = setting.edit();

        verifyPermissions();

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Bundle extras = getIntent().getExtras();

                // link = "https://youtube.com/c/setindia";
                // link = "https://youtube.com/channel/UC3d3X9S8v_k7DVdUIpV2hMw";
                // link = "https://youtube.com/user/Apple";

                if (extras != null) {

                    Log.d("The 2 :", "In Extra");
                    link = extras.getString(Intent.EXTRA_TEXT);

                    prefEditor.putString("playlistLink" + widgetId, link);
                    prefEditor.apply();
                    if (link != null) {

                        Log.d("The Link :", link);
                    }

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            editText.setText(link);
                        }
                    });
                }
            }
        });
    }

//
//    void connectGooglePlayBilling() {
//
//        billingClient.startConnection(new BillingClientStateListener() {
//            @Override
//            public void onBillingServiceDisconnected() {
//                connectGooglePlayBilling();
//            }
//
//            @Override
//            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
//                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
//                    Log.d(TAG, "Connected " + 0);
//                    getProducts();
//                }
//
//            }
//        });
//
//    }
//
//
//    void getProducts() {
//
//        List<String> skuList = new ArrayList<>();
//        skuList.add("nolimit_widgets");
//
//        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
//        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
//
//        billingClient.querySkuDetailsAsync(params.build(),
//                new SkuDetailsResponseListener() {
//
//                    @Override
//                    public void onSkuDetailsResponse(BillingResult billingResult,
//                                                     List<SkuDetails> skuDetailsList) {
//                        // Process the result.
//                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
//
//                            for (SkuDetails skuDetails : skuDetailsList) {
//                                if (skuDetails.getSku().equals("nolimit_widgets")) {
//                                    Log.d(TAG, "Connected6666 " + skuDetails.getPrice());
//                                    launchPurchaseFlow(skuDetails);
//                                }
//                            }
//                        } else {
//                            Log.d(TAG, "Connected6666 Failed To Complete Payment!");
//                        }
//
//                    }
//                });
//
//    }
//
//    void launchPurchaseFlow(SkuDetails skuDetails) {
//
//        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
//                .setSkuDetails(skuDetails)
//                .build();
//
//        billingClient.launchBillingFlow(MainActivity.this, billingFlowParams);
//    }
//
//    void verifyPayment(Purchase purchase) {
//
//
//        ConsumeParams consumeParams = ConsumeParams.newBuilder()
//                .setPurchaseToken(purchase.getPurchaseToken())
//                .build();
//
//        ConsumeResponseListener listener = new ConsumeResponseListener() {
//            @Override
//            public void onConsumeResponse(@NonNull BillingResult billingResult, @NonNull String s) {
//                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
//
//                    Log.d(TAG, purchase.getSkus().get(0) + " sku");
//
//                    if (purchase.getSkus().get(0).equals("nolimit_widgets")) {
//                        Log.d(TAG,  "Now Give Him unlimited Widgets");
//
//                        SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);
//                        SharedPreferences.Editor prefEditor = setting.edit();
//                        prefEditor.putBoolean("purchased", true);
//                        prefEditor.apply();
//                        // Unlock Widgets Here.
//                    }
//
//                }
//
//            }
//        };
//
//        billingClient.consumeAsync(consumeParams, listener);
//
//    }

    private void loadVideoAd() {
        AdRequest adRequest = new AdRequest.Builder().build();

        RewardedAd.load(this, "ca-app-pub-3940256099942544/5224354917", adRequest, new RewardedAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                super.onAdLoaded(rewardedAd);
                Log.d(TAG, "Video Ad was loaded.");
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d(TAG, loadAdError.toString());
            }
        });
    }

    private void loadInterstitialAd() {

        AdRequest adRequest = new AdRequest.Builder().build();

        // Banner Admob Test id : ca-app-pub-3940256099942544/6300978111
        // Banner Admob Real id : ca-app-pub-2188730752108749/2807622082
        // Interstitial Admob Test id : ca-app-pub-3940256099942544/1033173712
        // Interstitial Admob Real id : ca-app-pub-2188730752108749/2021347548

        InterstitialAd.load(this, "ca-app-pub-2188730752108749/2021347548", adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                super.onAdLoaded(interstitialAd);
                mInterstitialAd = interstitialAd;
                Log.d("Ad-Test", "onAdLoaded");

                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        super.onAdFailedToShowFullScreenContent(adError);
                        Log.d("Ad-Test", "The ad failed to show.");
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        super.onAdShowedFullScreenContent();
                        mInterstitialAd = null;
                        Log.d("Ad-Test", "The ad was shown.");
                    }

                    @Override
                    public void onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent();
                        Log.d("Ad-Test", "The ad was dismissed.");
                    }

                    @Override
                    public void onAdImpression() {
                        super.onAdImpression();
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d("Ad-Test", loadAdError.getMessage());
                mInterstitialAd = null;
            }
        });

    }

    private void addWidget() {

        SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);
        bought = setting.getBoolean("purchased", false);

        Context context = getApplicationContext();

        AppWidgetManager appWidgetManager = context.getSystemService(AppWidgetManager.class);

        ComponentName myProvider =
                new ComponentName(context, Playlist.class);

        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);

        int id;

        if (ids.length > 1) {
            id = ids[ids.length - 1] + 1;
            Log.d("idaddWidget", "ids.length > 1" + id);
        } else if (ids.length == 1) {
            id = ids[0] + 1;
            Log.d("id", "ids.length == 1 " + id);
        } else {
            id = 999;
            Log.d("id", "else" + id);
        }

      //  Toast.makeText(MainActivity.this, "Total Widgets Are:" + ids.length, Toast.LENGTH_LONG).show();

      //  int flag = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) ? PendingIntent.FLAG_MUTABLE : PendingIntent.FLAG_UPDATE_CURRENT;

   //     if (ids.length < 2) {
        Log.d(TAG, "before add widget");
            if (appWidgetManager.isRequestPinAppWidgetSupported()) {
                Log.d(TAG, "In add widget");
                Intent pinnedWidgetCallbackIntent = new Intent(context, Playlist.class);
                Log.d(TAG, "widget ID :" + id);
                PendingIntent successCallback = null;
                successCallback = PendingIntent.getActivity
                        (context, id, pinnedWidgetCallbackIntent, PendingIntent.FLAG_IMMUTABLE);

                appWidgetManager.requestPinAppWidget(myProvider, null, successCallback);
            }
//        } else if (ids.length >= 2) {
//            // TODO: PopUp View Here with button to buy infinite
//
//            if (bought) {
//                if (appWidgetManager.isRequestPinAppWidgetSupported()) {
//
//                    Intent pinnedWidgetCallbackIntent = new Intent(context, Playlist.class);
//
//                    PendingIntent successCallback = PendingIntent.getBroadcast(context, id,
//                            pinnedWidgetCallbackIntent, flag);
//
//                    appWidgetManager.requestPinAppWidget(myProvider, null, successCallback);
//                }
//            } else {
//                connectGooglePlayBilling();
//            }
//        }
    }

    public Boolean verifyPermissions() {

        if (!Settings.System.canWrite(getApplicationContext()))
        {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getApplicationContext().getApplicationInfo().packageName));
            startActivity(intent);
        }

        int permissionExternalMemory = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionExternalMemory != PackageManager.PERMISSION_GRANTED) {

            String[] STORAGE_PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

            ActivityCompat.requestPermissions(this, STORAGE_PERMISSIONS, 1);
            return false;
        }
        return true;
    }

    void downloadImage(String imageLink) {

        Context context = getApplicationContext();

//        if (!verifyPermissions()) {
//            Toast.makeText(MainActivity.this, "Storage Permission not granted, Please allow permission.", Toast.LENGTH_LONG).show();
//            return;
//        }

        Glide.with(context)
                .asBitmap()
                .load(imageLink)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {

                        try {
                            saveImage(resource);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        Toast.makeText(MainActivity.this, "Failed to Download image.", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void saveImage(Bitmap image) throws IOException {

        Context context = getApplicationContext();

        ComponentName myProvider =
                new ComponentName(context, Playlist.class);

        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);


        int widgetId;


        if (ids.length > 1) {
            widgetId = ids[ids.length - 1] + 1;
            Log.d("idsaveImage", "ids.length > 1" + widgetId);
        } else if (ids.length == 1) {
            widgetId = ids[0] + 1;
            Log.d("id", "ids.length == 1 " + widgetId);
        } else {
            widgetId = 999;
            Log.d("id", "else" + widgetId);
        }


        SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);

        SharedPreferences.Editor prefEditor = setting.edit();

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "any_picture_name");
        values.put(MediaStore.Images.Media.DESCRIPTION, "test Image taken");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        Uri uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        prefEditor.putString("thumbnailPath" + widgetId, String.valueOf(uri));
        prefEditor.apply();

        OutputStream outstream;
        try {
            outstream = getContentResolver().openOutputStream(uri);
            image.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
            outstream.close();
            //  Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        imageView.setImageBitmap(getBitmapFromUri(uri));
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    private void getJson() throws IOException {

        if (link.isEmpty() && editText.getText() != null) {
            // Toast.makeText(getApplicationContext(), editText.getText().toString(), Toast.LENGTH_SHORT).show();
            link = editText.getText().toString().trim();
        }

        if (!link.isEmpty()) {

            if (link.substring(20, 28).equals("playlist")) {
                //  Toast.makeText(getApplicationContext(), "in PlayListtt", Toast.LENGTH_SHORT).show();

                YoutubePlaylistAPI.p_list = "&id=" + link.substring(link.length() - 34);

                String url = YoutubePlaylistAPI.BASE_URL + YoutubePlaylistAPI.type + YoutubePlaylistAPI.part + YoutubePlaylistAPI.p_list + YoutubePlaylistAPI.KEY;

                Call<ModelPlaylist> data = YoutubePlaylistAPI.getPlaylistVideo().getYT(url);
                data.enqueue(new Callback<ModelPlaylist>() {
                    @Override
                    public void onResponse(Call<ModelPlaylist> call, Response<ModelPlaylist> response) {
                        if (!response.isSuccessful()) {
                            textViewResult.setText("Code: " + response.code());
                            return;
                        } else {


                            ModelPlaylist posts = response.body();
                            String vontent = "";

                            for (PlaylistDetail post : posts.getItems()) {

                                content += post.getSnippet().getTitle();
                                //  Toast.makeText(getApplicationContext(),"Titil" + content,Toast.LENGTH_SHORT).show();
                                vontent += post.getSnippet().getTitle() + "\n";


                                if (post.getSnippet().getThumbnails().getMaxres() != null) {
                                    Log.d("In:", "It's getMaxres() : ");
                                    imageUrl = post.getSnippet().getThumbnails().getMaxres().getUrl();
                                    buttonView.setEnabled(true);
                                } else if (post.getSnippet().getThumbnails().getHigh() != null) {
                                    Log.d("In:", "It's getHigh() : ");
                                    imageUrl = post.getSnippet().getThumbnails().getHigh().getUrl();
                                    buttonView.setEnabled(true);
                                } else if (post.getSnippet().getThumbnails().getMedium() != null) {
                                    Log.d("In:", "It's getMedium() : ");
                                    imageUrl = post.getSnippet().getThumbnails().getMedium().getUrl();
                                    buttonView.setEnabled(true);
                                } else if (post.getSnippet().getThumbnails().getNavo() != null) {
                                    Log.d("In:", "It's getNavo() : ");
                                    imageUrl = post.getSnippet().getThumbnails().getNavo().getUrl();
                                    buttonView.setEnabled(true);
                                }

                                textViewResult.append(content);
                            }

                            Context context = getApplicationContext();
                            ComponentName myProvider =
                                    new ComponentName(context, Playlist.class);

                            int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);

                            int widgetId;

                            Log.d("id length", "L" + ids.length);

                            if (ids.length > 1) {
                                widgetId = ids[ids.length - 1] + 1;
                                Log.d("idgetJson", "ids.length > 1" + widgetId);
                            } else if (ids.length == 1) {
                                widgetId = ids[0] + 1;
                                Log.d("id", "ids.length == 1 " + widgetId);
                            } else {
                                widgetId = 999;
                                Log.d("id", "else" + widgetId);
                            }

                            SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);

                            SharedPreferences.Editor prefEditor = setting.edit();
                            prefEditor.putString("playlistTitle" + widgetId, content);
                            //   prefEditor.putInt("totalVideos" + widgetId, totalVideos);
                            prefEditor.commit();

                            if (link.length() > 1) {
                                downloadImage(imageUrl);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelPlaylist> call, Throwable t) {
                        Log.d("3.0", "In Failure", t);
                    }
                });

            } else if (link.substring(20, 25).equals("watch") || (link.length() == 28 && link.substring(0, 17).equals("https://youtu.be/"))) {

                //https://youtube.com/watch?v=ajOwLRLqaJ8

                //  Toast.makeText(getApplicationContext(), "in Vedeo", Toast.LENGTH_SHORT).show();

                YoutubeVideoAPI.video_id = "&id=" + link.substring(link.length() - 11);

                String url = YoutubeVideoAPI.BASE_URL + YoutubeVideoAPI.type + YoutubeVideoAPI.part + YoutubeVideoAPI.video_id + YoutubeVideoAPI.KEY;


                Call<ModelVideo> data = YoutubeVideoAPI.getYoutubeVideo().getYT(url);
                data.enqueue(new Callback<ModelVideo>() {

                    @Override
                    public void onResponse(Call<ModelVideo> call, Response<ModelVideo> response) {
                        if (!response.isSuccessful()) {
                            textViewResult.setText("Code: " + response.code());
                            return;
                        } else {


                            ModelVideo posts = response.body();
                            String vontent = "";

                            for (VideoDetail post : posts.getItems()) {

                                content += post.getSnippet().getTitle();
                                vontent += post.getSnippet().getTitle() + "\n";


                                if (post.getSnippet().getThumbnails().getMaxre() != null) {
                                    Log.d("In:", "It's getMaxres() : ");
                                    imageUrl = post.getSnippet().getThumbnails().getMaxre().getUrl();
                                    buttonView.setEnabled(true);
                                } else if (post.getSnippet().getThumbnails().getHigh() != null) {
                                    Log.d("In:", "It's getHigh() : ");
                                    imageUrl = post.getSnippet().getThumbnails().getHigh().getUrl();
                                    buttonView.setEnabled(true);
                                } else if (post.getSnippet().getThumbnails().getMedium() != null) {
                                    Log.d("In:", "It's getMedium() : ");
                                    imageUrl = post.getSnippet().getThumbnails().getMedium().getUrl();
                                    buttonView.setEnabled(true);
                                } else if (post.getSnippet().getThumbnails().getDefaul() != null) {
                                    Log.d("In:", "It's getNavo() : ");
                                    imageUrl = post.getSnippet().getThumbnails().getDefaul().getUrl();
                                    buttonView.setEnabled(true);
                                }

                                Log.d("List of All 🤪", vontent);

                                textViewResult.append(content);
                            }

                            Context context = getApplicationContext();
                            ComponentName myProvider =
                                    new ComponentName(context, Playlist.class);

                            int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);

                            int widgetId;

                            Log.d("id length", "L" + ids.length);

                            if (ids.length > 1) {
                                widgetId = ids[ids.length - 1] + 1;
                                Log.d("idgetJson", "ids.length > 1" + widgetId);
                            } else if (ids.length == 1) {
                                widgetId = ids[0] + 1;
                                Log.d("id", "ids.length == 1 " + widgetId);
                            } else {
                                widgetId = 999;
                                Log.d("id", "else" + widgetId);
                            }

                            SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);

                            SharedPreferences.Editor prefEditor = setting.edit();
                            prefEditor.putString("playlistTitle" + widgetId, content);
                            prefEditor.commit();

                            if (link.length() > 1) {
                                downloadImage(imageUrl);
                            }


                        }


                    }

                    @Override
                    public void onFailure(Call<ModelVideo> call, Throwable t) {
                        Log.d("3.0", "In Failure", t);
                    }
                });


                //  Toast.makeText(getApplicationContext(), "It's Watch Means a Video Vats!", Toast.LENGTH_SHORT).show();

            } else if (link.substring(20, 24).equals("user") || link.substring(20, 21).equals("c") || link.substring(20, 27).equals("channel") || (link.substring(0, 20).equals("https://youtube.com/") && !(link.substring(20, 28).equals("playlist")) && !(link.substring(20, 25).equals("watch")))) {

                //  Toast.makeText(getApplicationContext(), "in Youtube Channel", Toast.LENGTH_SHORT).show();

                if (link.substring(20, 24).equals("user")) {
                    //    Toast.makeText(getApplicationContext(), "in USER", Toast.LENGTH_SHORT).show();
                    System.out.println("in User");
                    YoutubeChannelAPI.channel = link.substring(link.indexOf("user") + 5);
                    channelName = link.substring(link.indexOf("user/") + 5);

                    //    Toast.makeText(getApplicationContext(), "in USER :" + YoutubeChannelAPI.channel, Toast.LENGTH_SHORT).show();


                    modelidObservable()
                            .subscribeOn(Schedulers.io())
                            .flatMap(new Function<NewModelid, ObservableSource<ModelChannel>>() {
                                @Override
                                public ObservableSource<ModelChannel> apply(NewModelid newModelid) throws Exception {

                                    return getChannelObservable(newModelid);
                                }
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<ModelChannel>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    compositeDisposable.add(d);
                                }

                                @Override
                                public void onNext(ModelChannel modelChannel) {


                                    content = modelChannel.getItems().get(0).getSnippet().getTitle();


                                    if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getHigh() != null) {
                                        Log.d("In:", "It's getHigh() : ");

                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getHigh().getUrl();
                                        buttonView.setEnabled(true);
                                        //   Toast.makeText(getApplicationContext(), "High It Is!" + imageUrl, Toast.LENGTH_SHORT).show();
                                    } else if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getMedium() != null) {
                                        Log.d("In:", "It's getMedium() : ");
                                        //  Toast.makeText(getApplicationContext(), "Medium It Is!", Toast.LENGTH_SHORT).show();
                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getMedium().getUrl();
                                        buttonView.setEnabled(true);
                                    } else if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getDefaul() != null) {
                                        Log.d("In:", "It's getNavo() : ");
                                        //   Toast.makeText(getApplicationContext(), "Defaul It Is!", Toast.LENGTH_SHORT).show();
                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getDefaul().getUrl();
                                        buttonView.setEnabled(true);
                                    }


                                }

                                @Override
                                public void onError(Throwable e) {
                                    //  Toast.makeText(getApplicationContext(), "In Error" + e, Toast.LENGTH_SHORT).show();
                                    textViewResult.append(e.toString());
                                    Log.e(TAG, "onError: ", e);
                                }

                                @Override
                                public void onComplete() {


                                    Context context = getApplicationContext();

                                    ComponentName myProvider =
                                            new ComponentName(context, Playlist.class);

                                    int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);

                                    int widgetId;

                                    Log.d("id length", "L" + ids.length);

                                    if (ids.length > 1) {
                                        widgetId = ids[ids.length - 1] + 1;
                                        Log.d("idgetJson", "ids.length > 1" + widgetId);
                                    } else if (ids.length == 1) {
                                        widgetId = ids[0] + 1;
                                        Log.d("id", "ids.length == 1 " + widgetId);
                                    } else {
                                        widgetId = 999;
                                        Log.d("id", "else" + widgetId);
                                    }

                                    SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);

                                    SharedPreferences.Editor prefEditor = setting.edit();
                                    prefEditor.putString("playlistTitle" + widgetId, content);
                                    prefEditor.commit();

                                    if (link.length() > 1) {
                                        downloadImage(imageUrl);
                                    }


                                }
                            });


                } else if (link.substring(20, 27).equals("channel")) {
                    //  Toast.makeText(getApplicationContext(), "in Channel", Toast.LENGTH_SHORT).show();
                    System.out.println("in channel");
                    YoutubeChannelAPI.channel = /*"&id=" + */link.substring(link.indexOf("channel") + 8);


                    RetrofitClient.getMyAPI().getYT(YoutubeChannelAPI.channel)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<ModelChannel>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    compositeDisposable.add(d);
                                }

                                @Override
                                public void onNext(ModelChannel modelChannel) {
                                    content = modelChannel.getItems().get(0).getSnippet().getTitle();

                                    if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getHigh() != null) {
                                        Log.d("In:", "It's getHigh() : ");

                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getHigh().getUrl();
                                        buttonView.setEnabled(true);
                                        //     Toast.makeText(getApplicationContext(), "High It Is!" + imageUrl, Toast.LENGTH_SHORT).show();
                                    } else if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getMedium() != null) {
                                        Log.d("In:", "It's getMedium() : ");
                                        //    Toast.makeText(getApplicationContext(), "Medium It Is!", Toast.LENGTH_SHORT).show();
                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getMedium().getUrl();
                                        buttonView.setEnabled(true);
                                    } else if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getDefaul() != null) {
                                        Log.d("In:", "It's getNavo() : ");
                                        //   Toast.makeText(getApplicationContext(), "Defaul It Is!", Toast.LENGTH_SHORT).show();
                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getDefaul().getUrl();
                                        buttonView.setEnabled(true);
                                    }

                                }

                                @Override
                                public void onError(Throwable e) {
                                    //   Toast.makeText(getApplicationContext(), "In Error" + e, Toast.LENGTH_SHORT).show();
                                    textViewResult.append(e.toString());
                                    Log.e(TAG, "onError: ", e);
                                }

                                @Override
                                public void onComplete() {
                                    Context context = getApplicationContext();

                                    ComponentName myProvider =
                                            new ComponentName(context, Playlist.class);

                                    int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);

                                    int widgetId;

                                    Log.d("id length", "L" + ids.length);

                                    if (ids.length > 1) {
                                        widgetId = ids[ids.length - 1] + 1;
                                        Log.d("idgetJson", "ids.length > 1" + widgetId);
                                    } else if (ids.length == 1) {
                                        widgetId = ids[0] + 1;
                                        Log.d("id", "ids.length == 1 " + widgetId);
                                    } else {
                                        widgetId = 999;
                                        Log.d("id", "else" + widgetId);
                                    }

                                    SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);

                                    SharedPreferences.Editor prefEditor = setting.edit();
                                    prefEditor.putString("playlistTitle" + widgetId, content);
                                    prefEditor.commit();

                                    if (link.length() > 1) {
                                        downloadImage(imageUrl);
                                    }
                                }
                            });


                } else if (link.substring(20, 21).equals("c")) {

                    YoutubeChannelAPI.channel = /*"&id=" + */link.substring(link.indexOf("c/") + 2);
                    //   Toast.makeText(getApplicationContext(), "in c", Toast.LENGTH_SHORT).show();
                    System.out.println("in c");
                    channelName = link.substring(link.indexOf("c/") + 2);


                    modelidObservable()
                            .subscribeOn(Schedulers.io())
                            .flatMap(new Function<NewModelid, ObservableSource<ModelChannel>>() {
                                @Override
                                public ObservableSource<ModelChannel> apply(NewModelid newModelid) throws Exception {

                                    return getChannelObservable(newModelid);
                                }
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<ModelChannel>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    compositeDisposable.add(d);
                                }

                                @Override
                                public void onNext(ModelChannel modelChannel) {


                                    content = modelChannel.getItems().get(0).getSnippet().getTitle();


                                    if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getHigh() != null) {
                                        Log.d("In:", "It's getHigh() : ");

                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getHigh().getUrl();
                                        buttonView.setEnabled(true);
                                        //    Toast.makeText(getApplicationContext(), "High It Is!" + imageUrl, Toast.LENGTH_SHORT).show();
                                    } else if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getMedium() != null) {
                                        Log.d("In:", "It's getMedium() : ");
                                        //    Toast.makeText(getApplicationContext(), "Medium It Is!", Toast.LENGTH_SHORT).show();
                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getMedium().getUrl();
                                        buttonView.setEnabled(true);
                                    } else if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getDefaul() != null) {
                                        Log.d("In:", "It's getNavo() : ");
                                        //   Toast.makeText(getApplicationContext(), "Defaul It Is!", Toast.LENGTH_SHORT).show();
                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getDefaul().getUrl();
                                        buttonView.setEnabled(true);
                                    }


                                }

                                @Override
                                public void onError(Throwable e) {
                                    //   Toast.makeText(getApplicationContext(), "In Error" + e, Toast.LENGTH_SHORT).show();
                                    textViewResult.append(e.toString());
                                    Log.e(TAG, "onError: ", e);
                                }

                                @Override
                                public void onComplete() {


                                    Context context = getApplicationContext();

                                    ComponentName myProvider =
                                            new ComponentName(context, Playlist.class);

                                    int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);

                                    int widgetId;

                                    Log.d("id length", "L" + ids.length);

                                    if (ids.length > 1) {
                                        widgetId = ids[ids.length - 1] + 1;
                                        Log.d("idgetJson", "ids.length > 1" + widgetId);
                                    } else if (ids.length == 1) {
                                        widgetId = ids[0] + 1;
                                        Log.d("id", "ids.length == 1 " + widgetId);
                                    } else {
                                        widgetId = 999;
                                        Log.d("id", "else" + widgetId);
                                    }

                                    SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);

                                    SharedPreferences.Editor prefEditor = setting.edit();
                                    prefEditor.putString("playlistTitle" + widgetId, content);
                                    prefEditor.commit();

                                    if (link.length() > 1) {
                                        downloadImage(imageUrl);
                                    }

                                }
                            });


                } else if (link.substring(0, 20).equals("https://youtube.com/") && !(link.substring(20, 24).equals("user") && !(link.substring(20, 21).equals("c")) && !(link.substring(20, 27).equals("channel")))) {


                    YoutubeChannelAPI.channel = link.substring(link.indexOf("com/") + 4);
                    //  Toast.makeText(getApplicationContext(), "in Weird Name", Toast.LENGTH_SHORT).show();
                    System.out.println("in Weird Name");
                    channelName = link.substring(link.indexOf("com/") + 4);


                    modelidObservable()
                            .subscribeOn(Schedulers.io())
                            .flatMap(new Function<NewModelid, ObservableSource<ModelChannel>>() {
                                @Override
                                public ObservableSource<ModelChannel> apply(NewModelid newModelid) throws Exception {

                                    return getChannelObservable(newModelid);
                                }
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<ModelChannel>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    compositeDisposable.add(d);
                                }

                                @Override
                                public void onNext(ModelChannel modelChannel) {


                                    content = modelChannel.getItems().get(0).getSnippet().getTitle();


                                    if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getHigh() != null) {
                                        Log.d("In:", "It's getHigh() : ");

                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getHigh().getUrl();
                                        buttonView.setEnabled(true);
                                        //  Toast.makeText(getApplicationContext(), "High It Is!" + imageUrl, Toast.LENGTH_SHORT).show();
                                    } else if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getMedium() != null) {
                                        Log.d("In:", "It's getMedium() : ");
                                        //    Toast.makeText(getApplicationContext(), "Medium It Is!", Toast.LENGTH_SHORT).show();
                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getMedium().getUrl();
                                        buttonView.setEnabled(true);
                                    } else if (modelChannel.getItems().get(0).getSnippet().getThumbnails().getDefaul() != null) {
                                        Log.d("In:", "It's getNavo() : ");
                                        //   Toast.makeText(getApplicationContext(), "Defaul It Is!", Toast.LENGTH_SHORT).show();
                                        imageUrl = modelChannel.getItems().get(0).getSnippet().getThumbnails().getDefaul().getUrl();
                                        buttonView.setEnabled(true);
                                    }


                                }

                                @Override
                                public void onError(Throwable e) {
                                    //   Toast.makeText(getApplicationContext(), "In Error" + e, Toast.LENGTH_SHORT).show();
                                    textViewResult.append(e.toString());
                                    Log.e(TAG, "onError: ", e);
                                }

                                @Override
                                public void onComplete() {


                                    Context context = getApplicationContext();

                                    ComponentName myProvider =
                                            new ComponentName(context, Playlist.class);

                                    int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(myProvider);

                                    int widgetId;

                                    Log.d("id length", "L" + ids.length);

                                    if (ids.length > 1) {
                                        widgetId = ids[ids.length - 1] + 1;
                                        Log.d("idgetJson", "ids.length > 1" + widgetId);
                                    } else if (ids.length == 1) {
                                        widgetId = ids[0] + 1;
                                        Log.d("id", "ids.length == 1 " + widgetId);
                                    } else {
                                        widgetId = 999;
                                        Log.d("id", "else" + widgetId);
                                    }

                                    SharedPreferences setting = getSharedPreferences("PlaylistWidgetPreferences", MODE_PRIVATE);

                                    SharedPreferences.Editor prefEditor = setting.edit();
                                    prefEditor.putString("playlistTitle" + widgetId, content);
                                    prefEditor.commit();

                                    if (link.length() > 1) {
                                        downloadImage(imageUrl);
                                    }

                                }
                            });


                } else {
                    Toast.makeText(getApplicationContext(), "can't get data from this URL.", Toast.LENGTH_LONG).show();
                }



            } else {
                Toast.makeText(getApplicationContext(), "can't get data from this URL.", Toast.LENGTH_LONG).show();
            }

        } else {
              Toast.makeText(getApplicationContext(), "can't get data from this URL.", Toast.LENGTH_LONG).show();
        }


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // unregisterReceiver(playlistReceiver);
    }

}