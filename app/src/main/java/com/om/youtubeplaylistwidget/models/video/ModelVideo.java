package com.om.youtubeplaylistwidget.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.om.youtubeplaylistwidget.models.PlaylistDetail;

import java.util.List;

public class ModelVideo {

    @SerializedName("items")
    @Expose
    private List<VideoDetail> items;

    public ModelVideo(List<VideoDetail> items) {
        this.items = items;
    }

    public List<VideoDetail> getItems() {
        return items;
    }

    public void setItems(List<VideoDetail> items) {
        this.items = items;
    }
}
