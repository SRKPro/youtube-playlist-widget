package com.om.youtubeplaylistwidget.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MediumThumbnail {

    @SerializedName("url")
    @Expose
    private String url;

    public MediumThumbnail(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
