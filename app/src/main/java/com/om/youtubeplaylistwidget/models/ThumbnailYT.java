package com.om.youtubeplaylistwidget.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThumbnailYT {

//    public ThumbnailYT(MaxresThumbnail maxres, HighThumbnail high) {
//        this.maxres = maxres;
//        this.high = high;
//    }

    public ThumbnailYT(DefaultThumbnail defaul, MaxresThumbnail maxre, HighThumbnail high, MediumThumbnail medium) {
        this.defaul = defaul;
        this.maxre = maxre;
        this.high = high;
        this.medium = medium;
    }


    @SerializedName("default")
    @Expose
    private DefaultThumbnail defaul;

    @SerializedName("maxres")
    @Expose
    private MaxresThumbnail maxre;

    @SerializedName("high")
    @Expose
    private HighThumbnail high;

    @SerializedName("medium")
    @Expose
    private MediumThumbnail medium;


    public DefaultThumbnail getNavo() {
        return defaul;
    }

    public void setNavo(DefaultThumbnail defaul) {
        this.defaul = defaul;
    }


    public MediumThumbnail getMedium() {
        return medium;
    }

    public void setMedium(MediumThumbnail medium) {
        this.medium = medium;
    }


    public HighThumbnail getHigh() {
        return high;
    }

    public void setHigh(HighThumbnail high) {
        this.high = high;
    }


    public MaxresThumbnail getMaxres() {
        return maxre;
    }

    public void setMaxres(MaxresThumbnail maxre) {
        this.maxre = maxre;
    }
}
