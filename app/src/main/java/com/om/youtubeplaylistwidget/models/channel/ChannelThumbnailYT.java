package com.om.youtubeplaylistwidget.models.channel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChannelThumbnailYT {

    @SerializedName("default")
    @Expose
    private ChDefaultThumbnail defaul;

    @SerializedName("high")
    @Expose
    private ChHighThumbnail high;

    @SerializedName("medium")
    @Expose
    private ChMediumThumbnail medium;

    public ChannelThumbnailYT(ChDefaultThumbnail defaul, ChHighThumbnail high, ChMediumThumbnail medium) {
        this.defaul = defaul;
        this.high = high;
        this.medium = medium;
    }

    public ChDefaultThumbnail getDefaul() {
        return defaul;
    }

    public void setDefaul(ChDefaultThumbnail defaul) {
        this.defaul = defaul;
    }

    public ChHighThumbnail getHigh() {
        return high;
    }

    public void setHigh(ChHighThumbnail high) {
        this.high = high;
    }

    public ChMediumThumbnail getMedium() {
        return medium;
    }

    public void setMedium(ChMediumThumbnail medium) {
        this.medium = medium;
    }
}
