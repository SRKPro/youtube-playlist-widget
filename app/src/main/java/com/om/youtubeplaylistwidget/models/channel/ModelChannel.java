package com.om.youtubeplaylistwidget.models.channel;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelChannel {

    @SerializedName("items")
    @Expose
    private List<ChannelDetail> items;

    public ModelChannel(List<ChannelDetail> items) {
        this.items = items;
    }

    public List<ChannelDetail> getItems() {
        return items;
    }

    public void setItems(List<ChannelDetail> items) {
        this.items = items;
    }
}
