package com.om.youtubeplaylistwidget.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DefaultThumbnail {
    @SerializedName("url")
    @Expose
    private String url;

    public DefaultThumbnail(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
