package com.om.youtubeplaylistwidget.models.getid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.om.youtubeplaylistwidget.models.channel.ChannelSnippet;

public class IdDetail {

    @SerializedName("snippet")
    @Expose
    private IdSnippet snippet;

    public IdDetail(IdSnippet snippet) {
        this.snippet = snippet;
    }

    public IdSnippet getSnippet() {
        return snippet;
    }

    public void setSnippet(IdSnippet snippet) {
        this.snippet = snippet;
    }
}
