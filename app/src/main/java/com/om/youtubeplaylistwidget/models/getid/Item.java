package com.om.youtubeplaylistwidget.models.getid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {
    @Expose
    @SerializedName("snippet")
    public Snippet snippet;

    public Item(Snippet snippet) {
        this.snippet = snippet;
    }
}
