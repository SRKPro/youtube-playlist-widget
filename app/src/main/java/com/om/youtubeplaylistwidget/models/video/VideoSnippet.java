package com.om.youtubeplaylistwidget.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoSnippet {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("thumbnails")
    @Expose
    private VideoThumbnails thumbnails;

    public VideoSnippet(String title, VideoThumbnails thumbnails) {
        this.title = title;
        this.thumbnails = thumbnails;
    }

    public String getTitle() {
        return title;
    }

    public VideoThumbnails getThumbnails() {
        return thumbnails;
    }
}
