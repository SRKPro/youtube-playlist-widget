package com.om.youtubeplaylistwidget.models.getid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Snippet {
    @Expose
    @SerializedName("channelId")
    public String channelId;

    public Snippet(String channelId) {
        this.channelId = channelId;
    }
}
