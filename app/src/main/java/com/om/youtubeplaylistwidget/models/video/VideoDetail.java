package com.om.youtubeplaylistwidget.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.om.youtubeplaylistwidget.models.channel.ChannelSnippet;

public class VideoDetail {

    @SerializedName("snippet")
    @Expose
    private VideoSnippet snippet;

    public VideoDetail(VideoSnippet snippet) {
        this.snippet = snippet;
    }

    public VideoSnippet getSnippet() {
        return snippet;
    }
}
