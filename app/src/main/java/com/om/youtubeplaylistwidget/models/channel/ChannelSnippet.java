package com.om.youtubeplaylistwidget.models.channel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChannelSnippet {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("thumbnails")
    @Expose
    private ChannelThumbnailYT thumbnails;

    public ChannelSnippet(String title, ChannelThumbnailYT thumbnails) {
        this.title = title;
        this.thumbnails = thumbnails;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ChannelThumbnailYT getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(ChannelThumbnailYT thumbnails) {
        this.thumbnails = thumbnails;
    }
}
