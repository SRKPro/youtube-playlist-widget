package com.om.youtubeplaylistwidget.models.getid;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewModelid {

    @Expose
    @SerializedName("items")
    public List<Item> items;

    public NewModelid(List<Item> items) {
        this.items = items;
    }
}


