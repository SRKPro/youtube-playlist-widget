package com.om.youtubeplaylistwidget.models.channel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChannelDetail {

    @SerializedName("snippet")
    @Expose
    private ChannelSnippet snippet;

    public ChannelDetail(ChannelSnippet snippet) {
        this.snippet = snippet;
    }

    public ChannelSnippet getSnippet() {
        return snippet;
    }

    public void setSnippet(ChannelSnippet snippet) {
        this.snippet = snippet;
    }
}
