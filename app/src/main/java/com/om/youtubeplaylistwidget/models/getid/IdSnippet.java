package com.om.youtubeplaylistwidget.models.getid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdSnippet {

    @SerializedName("channelId")
    @Expose
    private String channelId;

    public IdSnippet(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
