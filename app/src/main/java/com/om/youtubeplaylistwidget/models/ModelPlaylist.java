package com.om.youtubeplaylistwidget.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelPlaylist {

    @SerializedName("items")
    @Expose
    private List<PlaylistDetail> items;

    public ModelPlaylist(List<PlaylistDetail> items) {
        this.items = items;
    }

    public List<PlaylistDetail> getItems() {
        return items;
    }

    public void setItems(List<PlaylistDetail> items) {
        this.items = items;
    }
}
