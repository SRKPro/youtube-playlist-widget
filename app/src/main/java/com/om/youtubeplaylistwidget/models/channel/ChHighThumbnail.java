package com.om.youtubeplaylistwidget.models.channel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChHighThumbnail {

    @SerializedName("url")
    @Expose
    private String url;

    public ChHighThumbnail(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
