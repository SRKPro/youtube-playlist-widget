package com.om.youtubeplaylistwidget.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaylistDetail {

    @SerializedName("snippet")
    @Expose
    private PlaylistSnippet snippet;

    @SerializedName("contentDetails")
    @Expose
    private TotalVideos contentDetails;

    public PlaylistDetail(PlaylistSnippet snippet, TotalVideos contentDetails) {
        this.snippet = snippet;
        this.contentDetails = contentDetails;
    }

    public PlaylistSnippet getSnippet() {
        return snippet;
    }

    public void setSnippet(PlaylistSnippet snippet) {
        this.snippet = snippet;
    }

    public TotalVideos getContentDetails() {
        return contentDetails;
    }

    public void setContentDetails(TotalVideos contentDetails) {
        this.contentDetails = contentDetails;
    }
}
