package com.om.youtubeplaylistwidget.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoMaxresThumbnail {
    @SerializedName("url")
    @Expose
    private String url;

    public VideoMaxresThumbnail(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
