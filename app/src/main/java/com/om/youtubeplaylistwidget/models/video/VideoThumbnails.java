package com.om.youtubeplaylistwidget.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.om.youtubeplaylistwidget.models.DefaultThumbnail;
import com.om.youtubeplaylistwidget.models.HighThumbnail;
import com.om.youtubeplaylistwidget.models.MaxresThumbnail;
import com.om.youtubeplaylistwidget.models.MediumThumbnail;

public class VideoThumbnails {

    @SerializedName("default")
    @Expose
    private VideoDefaultThumbnail defaul;

    @SerializedName("maxres")
    @Expose
    private VideoMaxresThumbnail maxre;

    @SerializedName("high")
    @Expose
    private VideoHighThumbnail high;

    @SerializedName("medium")
    @Expose
    private VideoMediumThumbnail medium;

    public VideoThumbnails(VideoDefaultThumbnail defaul, VideoMaxresThumbnail maxre, VideoHighThumbnail high, VideoMediumThumbnail medium) {
        this.defaul = defaul;
        this.maxre = maxre;
        this.high = high;
        this.medium = medium;
    }

    public VideoDefaultThumbnail getDefaul() {
        return defaul;
    }

    public VideoMaxresThumbnail getMaxre() {
        return maxre;
    }

    public VideoHighThumbnail getHigh() {
        return high;
    }

    public VideoMediumThumbnail getMedium() {
        return medium;
    }
}
