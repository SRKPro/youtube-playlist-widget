package com.om.youtubeplaylistwidget.models;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaxresThumbnail {

    @SerializedName("url")
    @Expose
    private String url;

    public MaxresThumbnail(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
