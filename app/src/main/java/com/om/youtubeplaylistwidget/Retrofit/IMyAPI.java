package com.om.youtubeplaylistwidget.Retrofit;

import com.om.youtubeplaylistwidget.MainActivity;
import com.om.youtubeplaylistwidget.models.channel.ModelChannel;
import com.om.youtubeplaylistwidget.models.getid.NewModelid;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface IMyAPI {

    // channels?part=snippet&id=UCb0VX3DhuwYY21ZFqf8LSqQ&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8

    // String query = MainActivity.idForAPI;

    //String val = "this";

    @GET("search?part=snippet&type=channel&fields=items/snippet/channelId&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8")
    Observable<NewModelid> getId(
            // @Path(value = "name") String name
            @Query("q") String name
    );

    @GET("channels?part=snippet&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8")
    Observable<ModelChannel> getYT(
            /*@Path(value = "id") String id/*, */@Query("id") String id
    );
    //Observable<NewModelid> getId(@Path("query") String query);
}
