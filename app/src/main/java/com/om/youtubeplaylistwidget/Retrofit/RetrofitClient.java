package com.om.youtubeplaylistwidget.Retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    //https://youtube.googleapis.com/youtube/v3/search?part=snippet&q=MuscleMadnessChannel&type=channel&fields=items/snippet/channelId&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8"

    public static final String BASE_URL = "https://youtube.googleapis.com/youtube/v3/";

    // private static Retrofit ourInstance;

//    public static Retrofit getInstance() {
////
////        if (ourInstance == null)
////            ourInstance = new Retrofit.Builder()
////                    .baseUrl("https://youtube.googleapis.com/youtube/v3/")
////                            //"search?part=snippet")
////                           // "&q=MuscleMadnessChannel&type=channel&fields=items/snippet/channelId&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8")
////                    .addConverterFactory(GsonConverterFactory.create())
////                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
////                    .build();
////
////            return ourInstance;
////
////
////    }

    private static Retrofit.Builder retrfitBuilder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrfitBuilder.build();

    private static IMyAPI myAPI = retrofit.create(IMyAPI.class);

    public static IMyAPI getMyAPI() {
        return myAPI;
    }

}
