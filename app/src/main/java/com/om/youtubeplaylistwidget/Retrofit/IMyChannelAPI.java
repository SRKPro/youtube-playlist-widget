package com.om.youtubeplaylistwidget.Retrofit;

import com.om.youtubeplaylistwidget.models.channel.ModelChannel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface IMyChannelAPI {

    @GET
    Observable<ModelChannel> getYT(@Url String url);

}
