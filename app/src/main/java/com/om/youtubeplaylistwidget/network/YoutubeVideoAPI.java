package com.om.youtubeplaylistwidget.network;

import com.om.youtubeplaylistwidget.models.video.ModelVideo;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

public class YoutubeVideoAPI {


    public static final String BASE_URL = "https://youtube.googleapis.com/youtube/v3/";
    public static final String type = "videos?";
    public static final String part = "part=snippet";
    public static String video_id = "";
    public static final String KEY = "&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8";


    // https://youtube.googleapis.com/youtube/v3/videos?part=snippet&id=DJSrJEFIxxM&key=[YOUR_API_KEY]

    public interface YoutubeVideo {
        @GET
        Call<ModelVideo> getYT(@Url String url);
    }

    private static YoutubeVideo youtubeVideo = null;

    public static YoutubeVideo getYoutubeVideo() {

        if (youtubeVideo == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            youtubeVideo = retrofit.create(YoutubeVideo.class);
        }

        return youtubeVideo;
    }

}
