package com.om.youtubeplaylistwidget.network;

import com.om.youtubeplaylistwidget.models.ModelPlaylist;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

public class YoutubePlaylistAPI {

    public static final String BASE_URL = "https://youtube.googleapis.com/youtube/v3/";
    public static final String type = "playlists?";
    public static final String part = "part=snippet&part=contentDetails";
    public static final String KEY = "&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8";
    public static String p_list = "";

    public interface PlaylistVideo {
        @GET
        Call<ModelPlaylist> getYT(@Url String url);
    }

    private static PlaylistVideo playlistVideo = null;

    public static PlaylistVideo getPlaylistVideo() {

        if (playlistVideo == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            playlistVideo = retrofit.create(PlaylistVideo.class);
        }

        return playlistVideo;
    }

}
