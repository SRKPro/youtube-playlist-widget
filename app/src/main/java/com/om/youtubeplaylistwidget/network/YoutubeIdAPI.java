// OM GANESHAY NAMAH //

package com.om.youtubeplaylistwidget.network;

//import com.om.youtubeplaylistwidget.models.getid.Modelid;

import com.om.youtubeplaylistwidget.models.getid.NewModelid;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

public class YoutubeIdAPI {


    public static final String BASE_URL = "https://youtube.googleapis.com/youtube/v3/";
    public static final String type = "search?";
    public static final String part = "part=snippet";
    public static String query = "";
    public static final String type2 = "&type=channel";
    public static final String fields = "&fields=items/snippet/channelId";
    public static final String KEY = "&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8";

    public interface ChannelId {
        @GET
        Observable<NewModelid> getYT(@Url String url);
    }


    private static ChannelId channelId = null;

    public static ChannelId getChannelId() {

        if (channelId == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            channelId = retrofit.create(ChannelId.class);
        }

        return channelId;
    }


}
