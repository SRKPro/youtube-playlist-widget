package com.om.youtubeplaylistwidget.network;

import com.om.youtubeplaylistwidget.models.channel.ModelChannel;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

public class YoutubeChannelAPI {

    //https://youtube.googleapis.com/youtube/v3/
    // channels?
    // part=snippet
    // &id=UCb0VX3DhuwYY21ZFqf8LSqQ
    // &key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8

    public static final String BASE_URL = "https://youtube.googleapis.com/youtube/v3/";
    public static final String type = "channels?";
    public static final String part = "part=snippet";
    public static final String KEY = "&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8";
    public static String channel = "";

//    public interface IMyChannelAPI {
//        @GET
//        Observable<ModelChannel> getYT(@Url String url);
//    }

    //private static YoutubeChannel youtubeChannel = null;

    private static Retrofit ourInstance;


    public static Retrofit getInstance() {

//        if (youtubeChannel == null) {
//            Retrofit retrofit = new Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//            youtubeChannel = retrofit.create(YoutubeChannel.class);
//        }

        //return youtubeChannel;

        if (ourInstance == null)
            ourInstance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    //"search?part=snippet")
                    // "&q=MuscleMadnessChannel&type=channel&fields=items/snippet/channelId&key=AIzaSyCQ5u2V1gHfTye0e81lSq-rxF1OPldb8A8")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();

        return ourInstance;
    }

}
