package com.om.youtubeplaylistwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.om.youtubeplaylistwidget.models.TotalVideos;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Playlist extends AppWidgetProvider {

    private static final String TAG = "PlayLst";
    public static String newdata;
    public static String thumbnailPath;
    public static String playlistLink;


    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) throws IOException {

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.playlist);

        SharedPreferences settings = context.getSharedPreferences("PlaylistWidgetPreferences", Context.MODE_PRIVATE);
        int[] allids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, Playlist.class));
        Log.d(TAG, "updateAppWidget: ");
        Log.d(TAG, "appWidgetId is: " + appWidgetId);

        if (appWidgetId == allids[0]) {
            addWidgetData(context);
        }

        if (allids.length > 1 && appWidgetId != 999) {
            newdata = settings.getString("playlistTitle" + appWidgetId, "Hi");
            thumbnailPath = settings.getString("thumbnailPath" + appWidgetId, "Hi");
            playlistLink = settings.getString("playlistLink" + appWidgetId, "empty link");
            Log.d("lin", playlistLink);
            Log.d("title", newdata);
            Log.d("thumbparth", thumbnailPath);
        }

        Uri uri = Uri.parse(playlistLink);

        Intent intent1 = new Intent(Intent.ACTION_VIEW);
        intent1.setData(uri);

        intent1.setAction(Intent.ACTION_VIEW);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                appWidgetId,
                intent1,
                PendingIntent.FLAG_IMMUTABLE);

        remoteViews.setOnClickPendingIntent(R.id.videoThumbnail, pendingIntent);

        remoteViews.setImageViewBitmap(R.id.videoThumbnail, getBitmapFromUri(thumbnailPath, context));
        remoteViews.setTextViewText(R.id.openPlayList, newdata);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }

    private static void addWidgetData(Context context) throws IOException {
        Log.d(TAG, "updateAppWidget: ");

        SharedPreferences settings = context.getSharedPreferences("PlaylistWidgetPreferences", Context.MODE_PRIVATE);
        newdata = settings.getString("playlistTitle" + 999, "Hi");
        thumbnailPath = settings.getString("thumbnailPath" + 999, "Hi");
        playlistLink = settings.getString("playlistLink" + 999, "empty link");
        Log.d(TAG, "link" + playlistLink);
        Log.d(TAG, "title" + newdata);
        Log.d(TAG, "thumbparth" + thumbnailPath);

        Uri uri = Uri.parse(playlistLink);
        Intent intent1 = new Intent(Intent.ACTION_VIEW);
        intent1.setData(uri);
        intent1.setAction(Intent.ACTION_VIEW);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.playlist);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 999, intent1, PendingIntent.FLAG_IMMUTABLE);
        remoteViews.setOnClickPendingIntent(R.id.videoThumbnail, pendingIntent);
        remoteViews.setImageViewBitmap(R.id.videoThumbnail, getBitmapFromUri(thumbnailPath, context));
        remoteViews.setTextViewText(R.id.openPlayList, newdata/* + "(" + totalVideos + ")"*/);
        AppWidgetManager mManager = AppWidgetManager.getInstance(context);
        ComponentName cn = new ComponentName(context, Playlist.class);
        mManager.updateAppWidget(cn, remoteViews);
        mManager.updateAppWidget(999, remoteViews);

    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        Log.d(TAG, "onUpdate: ");

        //  Log.d("1", "1In OnUpdate.");

        // SharedPreferences settings = context.getSharedPreferences("PlaylistWidgetPreferences", Context.MODE_PRIVATE);
        // int widgetId;

//        for (int i = 0; i < appWidgetIds.length; i++) {
//            Log.d("Id:", "1All Wid_IDs: " + appWidgetIds[i]);
//        }

//        for (int appwidid : appWidgetIds) {
//            Log.d("Id:", "All Wid_IDs: " + appwidid);
//
//        }

        for (int appWidgetId : appWidgetIds) {
            Log.d(TAG, "Id:"+"onUpdate: " + appWidgetId);
            // Log.d("Id:", "appWidgetIds[0]: " + appWidgetIds[0]);

            try {
                updateAppWidget(context, appWidgetManager, appWidgetId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    private static Bitmap getBitmapFromUri(String uri, Context context) throws IOException {

         Log.d(TAG, "In getBITMAPURI.");
         Log.d(TAG, "In : " + uri);
        ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(Uri.parse(uri), "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
//        if (fileDescriptor != null) {
//            Log.d("FileDics", "getBitmapFromUri: " + fileDescriptor.valid());
//        }
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        Log.d(TAG, "Here : " + image.getWidth());
        return image;
    }


    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);

        Log.d(TAG, "Enter Enable");

        int[] allids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, Playlist.class));
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        //  int widgetId;

        //  SharedPreferences settings = context.getSharedPreferences("PlaylistWidgetPreferences", Context.MODE_PRIVATE);

//        for (int appwidid : allids) {
//            Log.d("Id:", "All Wid_IDs: " + appwidid);
//
//        }

        for (int appWidgetId : allids) {

            //  int val = 0;

            //  Log.d("thumbparth", "allids[0]" + allids[0]);
            //  Log.d("thumbparth", "allids[allids.length - 1]" + allids[allids.length - 1]);

            if (allids.length == 1 || allids[0] == appWidgetId) {

                try {
                    addWidgetData(context);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {

                updateAppWidget(context, appWidgetManager, appWidgetId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}